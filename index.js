const rp = require('request-promise');
const WebSocket = require('ws');
const server = new WebSocket.Server({ port: 8001 });

const message = {
    track: '',
    artist: '',
    duration: 0,
    imageUrl: '',
    last: 0,
    error: ''
};

server.on('connection', function connection(ws) {
    ws.send(JSON.stringify(message));
});

const sendUpdate = () => {
    server.clients.forEach(function each(client) {
        if (client.readyState === WebSocket.OPEN) {
            client.send(JSON.stringify(message));
        }
    });
};

const requestTrack = () => {
    rp('https://rtfm.fm/np-status')
        .then((result) => {
            const currentTrack = result.split('\n')[1].split(' — ')[1];
            const currentArtist = result.split('\n')[1].split(' — ')[0];
            const duration = parseInt(result.split('\n')[4] - result.split('\n')[5]);
            const last = parseInt(result.split('\n')[5]);
            const trackId = parseInt(result.split('\n')[0]);
            const imageUrl = '';
            message.duration = duration;
            message.last = last;
            message.error = '';
            if (message.track != currentTrack) {
                message.track = currentTrack;
                message.artist = currentArtist;
                message.trackId = trackId;
                message.imageUrl = '';
                rp('http://ws.audioscrobbler.com/2.0/?method=track.getInfo&api_key=a3332093d4a785d2d87c6094d8c39900&artist=' + message.artist + '&track=' + message.track + '&format=json')
                    .then((response) => {
                        const data = JSON.parse(response);
                        if (data.track && data.track.album) {
                            const imageIndex = data.track.album.image.length - 1;
                            message.imageUrl = data.track.album.image[imageIndex]['#text'];
                        }
                        else { console.log('lastfm api error: ' + data.message) }

                        sendUpdate();
                        // console.log(currentTrack);
                    })
            };
        })
        .catch((err) => {
            message.track = '';
            message.artist = '';
            message.trackId = '';
            message.duration = 0;
            message.last = 0;
            message.error = err;
            sendUpdate();
            console.log(err);
        });
};

setInterval(requestTrack, 2000)